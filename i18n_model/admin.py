import copy
from django.conf import settings
from django.contrib.auth import get_permission_codename
from django.forms.models import BaseInlineFormSet


def i18n_formset_factory(languages=[]):
    # Create an ad-hoc form set
    class I18nFormSet(BaseInlineFormSet):
        def __init__(self, *args, **kwargs):
            kwargs['initial'] = [{'i18n_language': l} for l in languages]
            super(I18nFormSet, self).__init__(*args, **kwargs)

    return I18nFormSet


class I18nInlineMixin(object):
    max_num = len(settings.LANGUAGES) - 1

    def get_existing_translation(self, obj=None):
        if not obj or not obj.pk:
            return []
        return obj.translations.get_available_languages()

    def get_untranslated_languages(self, obj=None):
        if not obj or not obj.pk:
            return [lang[0] for lang in settings.LANGUAGES
                    if lang[0] != settings.LANGUAGE_CODE]
        else:
            translated = obj.translations.get_available_languages()
            return [lang[0] for lang in settings.LANGUAGES
                    if lang[0] not in translated
                    and lang[0] != settings.LANGUAGE_CODE]

    def get_extra(self, request, obj=None, **kwargs):
        existing_translations = self.get_existing_translation(obj)
        return len(settings.LANGUAGES) - 1 - len(existing_translations)

    def get_formset(self, request, obj=None, **kwargs):
        untranslated = self.get_untranslated_languages(obj)
        # Override the argument passed to superclass' get_formset method so
        # that our ad-hoc form set is used instead.
        kwargs['formset'] = i18n_formset_factory(untranslated)
        kwargs['extra'] = self.get_extra(request, obj)
        return super(I18nInlineMixin, self).get_formset(request, obj, **kwargs)

    # For inline forms to show up, Django checks whether the user
    # has permissions for the related model. In the most basic case,
    # `I18nModel`s wouldn't have their own permissions - so below we
    # make sure to inherit the permissions of the source model.
    
    def get_source_meta(self):
        source_field = self.model._meta.get_field_by_name('i18n_source')[0]
        return source_field.rel.to._meta

    def has_add_permission(self, request):
        opts = self.get_source_meta()
        codename = get_permission_codename('add', opts)
        return request.user.has_perm("%s.%s" % (opts.app_label, codename))

    def has_change_permission(self, request, obj=None):
        opts = self.get_source_meta()
        codename = get_permission_codename('change', opts)
        return request.user.has_perm("%s.%s" % (opts.app_label, codename))

    def has_delete_permission(self, request, obj=None):
        opts = self.get_source_meta()
        codename = get_permission_codename('delete', opts)
        return request.user.has_perm("%s.%s" % (opts.app_label, codename))


def merge_form(form_1, form_2):
    """ Adds the fields, initial data, and errors from form_2
        into form_1, and returns form_1. """

    prefix = form_2.prefix + "-" if form_2.prefix else ""

    form_1.base_fields.update({
        prefix + field_name: field
        for field_name, field in form_2.base_fields.items()})

    form_1.initial.update({
        prefix + field_name: field
        for field_name, field in form_2.initial.items()})

    if form_1._errors is not None:
        form_1._errors.update({prefix + field_name: error
            for field_name, error in form_2._errors.items()})

    # Update fields from base_fields - same as in Form constructor
    form_1.fields = copy.deepcopy(form_1.base_fields)

    return form_1


class SideBySideI18nAdminMixin(object):

    def _process_i18n_inline(self, context, inline):
        """ Removes translation inlines from the template context,
            and moves their fields into the main form. """

        new_fields = ()

        # Merge each form of the formset
        for form in inline.formset.forms:
            context['adminform'].form = merge_form(
                context['adminform'].form, form)

            # Add extra lines to the fieldset for the extra fields
            for field in form.base_fields.keys():
                if field == 'i18n_language':
                    new_fields += (form.prefix + "-" + field,)
                    continue

                for fieldset in context['adminform'].fieldsets:
                    extra = []
                    for i, line in enumerate(fieldset[1]['fields']):
                        match = (type(line) is str and line == field
                                 or field in line)
                        if match:
                            extra += [(i + 1, form.prefix + "-" + field)]

                    field_list = list(fieldset[1]['fields'])
                    for i, line in reversed(extra):
                        field_list.insert(i, line)
                    fieldset[1]['fields'] = tuple(field_list)

        # Merge management form
        mform = inline.formset.management_form
        context['adminform'].form = merge_form(
            context['adminform'].form, mform)

        # Add fieldset for management form and language selector fields
        new_fields += tuple([(mform.prefix + "-" + f)
                      for f in mform.base_fields.keys()])

        context['adminform'].fieldsets.append((None, {'classes': ('hidden',),
                                                      'fields': new_fields}))
        
        return context

    def render_change_form(self, request, context, **kwargs):
        """ Processes the render context, altering the form to be rendered -
            but not the the form instance which will parse the input upon
            submission. Instead, we try to maintain compatibility with the
            original form+formset, by having the altered form POST exactly
            the same data. """

        inlines = []

        # Separate translation inlines from others
        for inline in context['inline_admin_formsets']:
            if isinstance(inline.opts, I18nInlineMixin):
                context = self._process_i18n_inline(context, inline)
            else:
                inlines.append(inline)

        context['inline_admin_formsets'] = inlines

        return super(SideBySideI18nAdminMixin, self). \
            render_change_form(request, context, **kwargs)